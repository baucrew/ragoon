class Ragoon::Services::Admin < Ragoon::Services
  XML_NS = 'http://wsdl.cybozu.co.jp/admin/2008'

  def admin_get_user_id_by_login_name(login_name)
    action_name = 'AdminGetUserIdByLoginName'

    body_node = Ragoon::XML.create_node(action_name)
    parameter_node = Ragoon::XML.create_node('parameters')
    login_node = Ragoon::XML.create_node('login_name')
    login_node.add_child(login_name)
    parameter_node.add_child(login_node)
    body_node.add_child(parameter_node)

    client.request(action_name, body_node)
    client.result_set.xpath('//userId').first.content.strip.to_i
  end
end
