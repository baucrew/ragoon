class Ragoon::Services::Util < Ragoon::Services
  XML_NS = 'http://wsdl.cybozu.co.jp/util_api/2008'

  def util_login(options = {})
    action_name = 'UtilLogin'

    body_node = Ragoon::XML.create_node(action_name)
    parameter_node = Ragoon::XML.create_node('parameters')
    login_node = Ragoon::XML.create_node('login_name')
    login_node.add_child(options[:login_name])
    password_node = Ragoon::XML.create_node('password')
    password_node.add_child(options[:password])
    parameter_node.add_child(login_node)
    parameter_node.add_child(password_node)
    body_node.add_child(parameter_node)

    client.request(action_name, body_node)

    res = client.result_set.xpath('//cookie')
    res[0].children.to_s.strip.split(/[=;]/).second
  end

  def util_logout
    action_name = 'UtilLogout'

    body_node = Ragoon::XML.create_node(action_name)
    client.request(action_name, body_node)

    res = client.result_set.xpath('//status')
    res.to_s.match(/Logout/)
  end

  def util_get_login_user_id
    action_name = 'UtilGetLoginUserId'

    body_node = Ragoon::XML.create_node(action_name)
    client.request(action_name, body_node)

    client.result_set.xpath('//user_id').to_i
  end

  def util_get_request_token
    action_name = 'UtilGetRequestToken'

    body_node = Ragoon::XML.create_node(action_name)
    client.request(action_name, body_node)

    res = client.result_set.xpath('//request_token')
    res.first.children.to_s.strip
  end
end
